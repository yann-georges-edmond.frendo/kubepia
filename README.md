# KubEpia

Une CLI pour le contrôle d'un cluster kubernetes en python.

Permet de démarrer des "VM" et des grilles condor avec un accès SSH.

## Mode d'emploi 

Nécéssite python 3.6 ou suppérieur et un cluster Kubernetes avec les droits d'administration.

Installer les requirements : pip install -r requirements.txt

Lancement du manager avec main.sh

# Fonctionalitées

## KubeVm

Permet la création d'une pseudo VM sur le cluster.
Gère l'attribution CPU,RAM et GPU avec détection automatique des ressources disponibles.

Toutes les vm sont créer dans un namespace commun nommé "vm"

La vm est accessible via ssh grace a un NodePort.

Identifiants: root:root

Attention ceci n'est pas addapté à des environement ouvert mais uniquement pour des besoins internes de calculs pour des raisons évidente de sécurité!

## KubeCondor

Permet la création d'une grille de calcul HTcondor sur le cluster.
Gère l'attribution CPU,RAM avec détection automatique des ressources disponibles.

Touts les pods sont créer dans un namespace commun définit par l'utilisateur

La vm est accessible via ssh grace a un NodePort.

Identifiants: root:root

Attention ceci n'est pas addapté à des environement ouvert mais uniquement pour des besoins internes de calculs pour des raisons évidente de sécurité!
