import yaml

from kubernetes import client, config, utils
import random

import os


class Grille:

    def __init__(self,name,nombre_nodes,cpu_nodes,ram_nodes,image=None):

        config.load_kube_config()
        v1 = client.CoreV1Api()
        list_namespace = v1.list_namespace(watch=False) #Liste tout les namespace pour vérifier le nom

        list_nodeport = [] # liste des nodeport utilisé
        api_list_nodeport = v1.list_service_for_all_namespaces(watch=False)
        for i in api_list_nodeport.items:
            for j in i.spec.ports:
                list_nodeport.append(j._node_port)
        port = 30000
        while port in list_nodeport: #On tire un port non utilisé de la plage
            port = random.randint(30000,32768)

        self.nombre_nodes = nombre_nodes
        self.cpu_nodes = str(cpu_nodes)
        self.ram_nodes = str(ram_nodes) + '.0Gi' #Init ram par nodes en GO
        self.port = port
        self.image = image

        for i in list_namespace.items:
            if name == i.metadata.name:
                raise NameError("Nom de namespace déja utilisé veuillez en choisir un autre")

        self.name = name

        
    
    def create_yaml(self):

        with open('htcondor/htcondor.yaml', 'r') as file:
            prime_service = yaml.safe_load_all(file)

            for k in prime_service:

                if k["kind"] == "Namespace":
                    namespace = k
                if k["kind"] == "Pod":
                    cm_pod = k
                if k["kind"] == "Deployment":
                    deployement = k
                if k["metadata"]["name"] == "htcondor-central-manager":
                    cm_service = k

                if k["metadata"]["name"] == "htcondor-central-manager-ssh":
                    ssh_service = k

        namespace["metadata"]["name"] = self.name # Changement du namespace
        cm_pod["metadata"]["namespace"] = self.name
        deployement["metadata"]["namespace"] = self.name
        cm_service["metadata"]["namespace"] =self.name
        ssh_service["metadata"]["namespace"] = self.name
        
        deployement["spec"]["replicas"] = self.nombre_nodes # Nombre de nodes

        deployement["spec"]["template"]["spec"]["containers"][0]["resources"]["requests"]["cpu"] = self.cpu_nodes #Nombres de CPU
        deployement["spec"]["template"]["spec"]["containers"][0]["resources"]["limits"]["cpu"] = self.cpu_nodes

        deployement["spec"]["template"]["spec"]["containers"][0]["resources"]["requests"]["memory"] = self.ram_nodes #Nombres de CPU
        deployement["spec"]["template"]["spec"]["containers"][0]["resources"]["limits"]["memory"] = self.ram_nodes

        ssh_service["spec"]["ports"][0]["nodePort"] = self.port

        if self.image is not None:
            deployement["spec"]["template"]["spec"]["containers"][0]['image'] = self.image

        if not os.path.exists(path = './htcondor_yaml_files'):
            os.makedirs('./htcondor_yaml_files')
                
        
        with open(f"./htcondor_yaml_files/{self.name}.yaml","w+") as file:
        
            yaml.dump_all(
                [namespace,cm_pod,deployement,cm_service,ssh_service],
                file,
                default_flow_style=False)

    def create_grille(self):
        """Permet de créer la grille en fonction du fichier YAML créer"""
        config.load_kube_config()
        k8s_client = client.ApiClient()
        yaml_file = f"./htcondor_yaml_files/{self.name}.yaml"
        utils.create_from_yaml(k8s_client, yaml_file)
