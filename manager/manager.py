from __future__ import print_function, unicode_literals
from kubernetes import client, config
from matplotlib.pyplot import flag
from pyfiglet import Figlet
import os

from InquirerPy import prompt, inquirer
from prompt_toolkit.validation import ValidationError, Validator

import re
from termcolor import colored

from htcondor import Grille
from vm import Vm
from loader import Loader
from time import sleep 



class NameSpaceValidator(Validator):

    def validate(self, document):

        pattern = re.compile(r"^[a-z,-]+$") # regex namespace kubernetes

        config.load_kube_config()
        v1 = client.CoreV1Api()
        list_namespace = v1.list_namespace(watch=False)

        for i in list_namespace.items:
            if document.text == i.metadata.name:
                raise ValidationError(message="Nom de namespace déja utilisé veuillez en choisir un autre",
                cursor_position=len(document.text)
                )
        if  not pattern.match(document.text):
            raise ValidationError(message="Nom de namespace invalide uniquement a-z et -",
                cursor_position=len(document.text)
                )

class VmNameValidator(Validator):

    def validate(self, document):

        pattern = re.compile(r"^[a-z,-]+$") # regex namespace kubernetes

        config.load_kube_config()
        v1 = client.CoreV1Api()
        list_namespace = v1.list_namespaced_pod(namespace="vm")

        for i in list_namespace.items:
            if document.text == i.metadata.name:
                raise ValidationError(message="Nom de vm déja utilisé veuillez en choisir un autre",
                cursor_position=len(document.text)
                )
        if  not pattern.match(document.text):
            raise ValidationError(message="Nom de vm invalide uniquement a-z et -",
                cursor_position=len(document.text)
                )

class NumberValidator(Validator):
    def validate(self, document):
        try:
            int(document.text)
        except ValueError:
            raise ValidationError(
                message='Please enter a number',
                cursor_position=len(document.text))  # Move cursor to end

class FloatNumberValidator(Validator):
    def validate(self, document):
        try:
            float(document.text)
        except ValueError:
            raise ValidationError(
                message='Please enter a number',
                cursor_position=len(document.text))  # Move cursor to end

class RessourceCpuValidator(Validator):
    def __init__(self,tot_cpu,numberNode=1):
        self.numberNode = numberNode
        self.tot_cpu = tot_cpu
    def validate(self, document):
        try:
            float(document.text)
        except ValueError:
            raise ValidationError(
                message='Please enter a number',
                cursor_position=len(document.text))  # Move cursor to end
        if int(self.numberNode) * float(document.text) > self.tot_cpu:
            raise ValidationError(
                message=f'Demande en CPU trop élevé. Demande: {int(self.numberNode) * float(document.text)} Maximum: {self.tot_cpu}',
                cursor_position=len(document.text))  # Move cursor to end

class RessourceMemValidator(Validator):
    def __init__(self,tot_mem,numberNode=1):
        self.numberNode = numberNode
        self.tot_mem = tot_mem
    def validate(self, document):
        try:
            float(document.text)
        except ValueError:
            raise ValidationError(
                message='Please enter a number',
                cursor_position=len(document.text))  # Move cursor to end
        if int(self.numberNode) * float(document.text) > self.tot_mem:
            raise ValidationError(
                message=f'Demande en mémoire trop élevé.Demande: {int(self.numberNode) * float(document.text)} Maximum: {self.tot_mem}',
                cursor_position=len(document.text))  # Move cursor to end

def vm():

    f = Figlet(font='slant')
    print (f.renderText('KubeVm'))

    try:

        config.load_kube_config()
        v1 = client.CoreV1Api()

        list_node = v1.list_node()
        listpod = v1.list_pod_for_all_namespaces()

        list_ns = v1.list_namespace()

        ns_exist = False

        for ns in list_ns.items:

            if ns.metadata.name == "vm":
                ns_exist = True
        
        if ns_exist != True:

            v1.create_namespace(client.V1Namespace(metadata=client.V1ObjectMeta(name="vm")))


        dic_node = {}
        list_gpu = []

        for node in list_node.items:

            dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"] = {} #Un dic par node

            dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"]["memory_request"] = 0
            dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"]["cpu_request"] = 0

            dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"]["cpu"] = node.status.allocatable["cpu"]
            dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"]["memory"] = node.status.allocatable["memory"]

            try:

                dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"]["gpu"] = node.status.allocatable['nvidia.com/gpu']
                dic_node[f"{node.metadata.labels['kubernetes.io/hostname']}"]["gpu_type"] = node.metadata.labels["nvidia.com/gpu.product"]

                if node.metadata.labels["nvidia.com/gpu.product"] not in list_gpu:# On ajjoute à la liste des gpu si pas déja présent
                    list_gpu.append(node.metadata.labels["nvidia.com/gpu.product"])



            except:
                pass

            

        for pods in listpod.items:

            try: # Memory
                if pods.spec.containers[0].resources.requests["memory"][-2] == "M":
                    dic_node[pods.spec.node_name]["memory_request"] = dic_node[pods.spec.node_name]["memory_request"] + (float(pods.spec.containers[0].resources.requests["memory"][:-2])/1000)
                else:
                    dic_node[pods.spec.node_name]["memory_request"] = dic_node[pods.spec.node_name]["memory_request"] + float(pods.spec.containers[0].resources.requests["memory"][:-2])
            except:
                pass

            try: # CPU
                if pods.spec.containers[0].resources.requests["cpu"][-1] == "m":
                    dic_node[pods.spec.node_name]["cpu_request"] = dic_node[pods.spec.node_name]["cpu_request"] + float("O." + pods.spec.containers[0].resources.requests["cpu"][-1])
                else:
                    dic_node[pods.spec.node_name]["cpu_request"] = dic_node[pods.spec.node_name]["cpu_request"] + float(pods.spec.containers[0].resources.requests["cpu"])
            except:
                pass

    except:
        raise ConnectionError("Erreur lors du contact avec la grille veuillez vérifier votre fichier de configuration et l'état de la grille")


    choice = ""

    while True:
       
        print(colored("Bienvenue sur KubeVm!\n\n", 'green', attrs=['bold']))
        print(colored("Attention à bien respecter la limite de ressource disponible!\n\n", 'red', attrs=['bold']))

        choice = inquirer.select(
            message="Que voulez vous faire?",
            choices=["Lancer Vm", "Controler l'état des nodes"],
        ).execute()
    

        if choice == "Controler l'état des nodes":
            for node in dic_node:
                print("---")
                print("Name: " + node)
                print("CPU: " + dic_node[node]["cpu"])
                print("CPU disponible: " + str(int(dic_node[node]["cpu"]) - dic_node[node]["cpu_request"]))
                print("Memory: " + str(round(int(dic_node[node]["memory"][:-2]) / 1000000,2)) + " Go")
                print("Memory disponible: " + str(round(int(dic_node[node]["memory"][:-2]) / 1000000,2) - dic_node[node]["memory_request"])+ " Go")

                try:
                    print("GPU: " + dic_node[node]["gpu"])
                    print("Type GPU: " + dic_node[node]["gpu_type"])

                except:
                    pass
            print("\n\---")
            input("Press Enter to continue...")
            os.system('clear')
            print (f.renderText('KubeVm'))
        else:
            break

    tot_cpu = 0
    tot_memory = 0   

    for node in dic_node: # Calcul de la mémoire et du CPU disponible sur la node la plus free

        if int(dic_node[node]["cpu"]) - dic_node[node]["cpu_request"] > tot_cpu:
            tot_cpu = int(dic_node[node]["cpu"]) - dic_node[node]["cpu_request"]

            if round(int(dic_node[node]["memory"][:-2]) / 1000000,2) - dic_node[node]["memory_request"] > tot_memory:
                tot_memory = round(int(dic_node[node]["memory"][:-2]) / 1000000,2) - dic_node[node]["memory_request"]


 
    question = [
        {
        'type': 'input',
        'name': 'name',
        'message': 'Nom du vm',
        'validate': VmNameValidator(),
        },
        {
        'type': 'input',
        'name': 'CPU',
        'message': 'CPU de la vm',
        'validate': RessourceCpuValidator(tot_cpu),
        'default' : "2"
        },
        {
        'type': 'input',
        'name': 'memory',
        'message': 'Mémoire de la vm en Go',
        'validate': RessourceMemValidator(tot_memory),
        'default' : "4"
        },
        {
        'type': 'input',
        'name': 'image',
        'message': 'Image de la Vm',
        'default' : "Default"
        },
    ]

    result = prompt(question)

    for node in list_node.items: # Calcul du nombre de GPu disponible par node on prend le plus grand 
        
        if "nvidia.com/gpu.product" in node.metadata.labels:

            gpu_available = int(node.status.allocatable['nvidia.com/gpu'])
            if node.metadata.labels["nvidia.com/gpu.product"] not in list_gpu:
                list_gpu.append(node.metadata.labels["nvidia.com/gpu.product"])
 
            for pod in listpod.items:
                if pod.spec.node_name == node.metadata.name:
                    for container in pod.spec.containers:
                        if container.resources.requests != None:
                            if 'nvidia.com/gpu' in container.resources.requests:
                                gpu_available = gpu_available - int(container.resources.requests['nvidia.com/gpu'])
            
            if gpu_available == 0:
                list_gpu.remove(node.metadata.labels["nvidia.com/gpu.product"])
                                    
    if len(list_gpu) == 0 or gpu_available == 0: # Si pas de GPU détecté
        choice = "Non"
        print(colored("Pas de GPU disponible dans ce cluster...", 'red', attrs=['bold']))

    else:
        choice = inquirer.select(
            message="Voulez vous un GPU?",
            choices=["Oui","Non"],
        ).execute()
        if choice == "Oui":

            gpu_choice = inquirer.select(
                message="Veuillez sélectionner le type de GPU:",
                choices=list_gpu,
            ).execute()

            gpu_available = 0

            for node in list_node.items: # Calcul du nombre de GPU disponible par node on prend le plus grand 
                if "nvidia.com/gpu.product" in node.metadata.labels:
                    if node.metadata.labels["nvidia.com/gpu.product"] == gpu_choice and gpu_available < int(node.status.allocatable['nvidia.com/gpu']):
                        gpu_available = int(node.status.allocatable['nvidia.com/gpu'])

                        for pod in listpod.items:     
                            if pod.spec.node_name == node.metadata.name:
                                for container in pod.spec.containers:
                                    if container.resources.requests != None:
                                        if 'nvidia.com/gpu' in container.resources.requests:
                                            gpu_available = gpu_available - int(container.resources.requests['nvidia.com/gpu'])
                
                
            
            number_gpu = inquirer.select(
                message="Combien de GPU",
                choices=range(1,gpu_available + 1),
            ).execute()
            

    


    print("\n")
    loader = Loader(colored("Création de la vm...", 'green', attrs=['bold'])).start()


    if result["image"] == "Default": # Image par défault

        if choice == "Oui":
            d = Vm(result["name"],int(result["CPU"]),int(result["memory"]),GPU_num=number_gpu,GPU_name=gpu_choice)
        else:
            d = Vm(result["name"],int(result["CPU"]),int(result["memory"]))

    else:
     
        if choice == "Oui":
            d = Vm(result["name"],int(result["CPU"]),int(result["memory"]),image = result["image"],GPU_num=number_gpu,GPU_name=gpu_choice)
        else:
            d = Vm(result["name"],int(result["CPU"]),int(result["memory"]),image = result["image"])
    


    d.create_yaml()
    d.create_vm()


    ready = False

    for k in range(30): # Attente 30 sc max 

        result = v1.list_namespaced_pod(namespace="vm")

        ready = True

        for pods in result.items:
            if pods.metadata.name == d.name:
                try:
                    if pods.status.container_statuses[0].state.running == None:
                        ready = False
                        sleep(1)
                except:
                    ready = False
                    sleep(1)
            
        if ready:
            loader.stop()
            print("🎉🎉🎉 " + colored("KubeVm est prêt vous pouvez accéder a la vm via ssh: \n\n", 'green', attrs=['bold']) + colored(f"ssh root@{list_node.items[0].status.addresses[0].address} -p {d.port} \n\n", 'red', attrs=['bold']) + colored(f"mdp: root\n\n", 'red', attrs=['bold']))
            return(1)

    print(colored("\n\nUne erreur est survenue veuillez vérifier la grille...", 'red', attrs=['bold']))


def htcondor():

    questions_1 = [
        {
        'type': 'input',
        'name': 'namespace',
        'message': 'Name of Namespace',
        'validate': NameSpaceValidator()
        },
        {
        'type': 'input',
        'name': 'nombreNodes',
        'message': 'Nombre de worker',
        'validate': NumberValidator()
        },

    ]

    f = Figlet(font='slant')
    print (f.renderText('KubeCondor'))

    try:

        config.load_kube_config()

        v1 = client.CoreV1Api()

        listnode = v1.list_node()
        listpod = v1.list_pod_for_all_namespaces()

        tot_cpu = 0
        tot_memory = 0

        for k in range(len(listnode.items)):
            tot_cpu = tot_cpu + int(listnode.items[k].status.allocatable["cpu"])

            tot_memory = tot_memory + int(listnode.items[k].status.allocatable["memory"][:-2])
        
        cpu_request = 0
        memory_request = 0

        for k in range(len(listpod.items)):

            try: # Memory
                if listpod.items[k].spec.containers[0].resources.requests["memory"][-2] == "M":
                    memory_request = memory_request + (float(listpod.items[k].spec.containers[0].resources.requests["memory"][:-2])/1000)
                else:
                    memory_request = memory_request + float(listpod.items[k].spec.containers[0].resources.requests["memory"][:-2])
            except:
                pass

            try: # CPU
                if listpod.items[k].spec.containers[0].resources.requests["cpu"][-1] == "m":
                    cpu_request = cpu_request + float("O." + listpod.items[k].spec.containers[0].resources.requests["cpu"][-1])
                else:
                    cpu_request = cpu_request + float(listpod.items[k].spec.containers[0].resources.requests["cpu"])
            except:
                pass

        tot_cpu = tot_cpu - cpu_request
        tot_memory = round(tot_memory / 1000000,2) - memory_request

    except:
        raise ConnectionError("Erreur lors du contact avec la grille veuillez vérifier votre fichier de configuration et l'état de la grille")

    print(colored("Bienvenue sur kubecondor!\n\n", 'green', attrs=['bold']))
    print(colored("Etat acctuel de la grille:\n", 'green', attrs=['bold']))
    print(colored("CPU disponible: ", 'green', attrs=['bold']) + colored(f"{tot_cpu - 3}", 'red', attrs=['bold']))
    print(colored("Mémoire disponible: ", 'green', attrs=['bold']) + colored(f"{round(tot_memory,2)}Gb\n\n", 'red', attrs=['bold']))

    result_1 = prompt(questions_1)

    questions_2 = [
        {
        'type': 'input',
        'name': 'CPU',
        'message': 'Nombre de CPU par worker',
        'validate': RessourceCpuValidator(tot_cpu,result_1['nombreNodes']),
        'default' : "1"
        },
                {
        'type': 'input',
        'name': 'memory',
        'message': 'Memoire en GO par worker',
        'validate': RessourceMemValidator(tot_memory,result_1['nombreNodes']),
        'default' : "1"
        },{
        'type': 'input',
        'name': 'image',
        'message': 'Image des dockers workers',
        'default' : "Default"
        },
    ]

    result_2 = prompt(questions_2)


    print("\n")
    loader = Loader(colored("Création de la grille...", 'green', attrs=['bold'])).start()


    if result_2["image"] == "Default": # Image par défault

        g = Grille(result_1["namespace"],int(result_1["nombreNodes"]),int(result_2["CPU"]),int(result_2["memory"]))
    else:
        g = Grille(result_1["namespace"],int(result_1["nombreNodes"]),int(result_2["CPU"]),int(result_2["memory"]),result_2["image"])

    g.create_yaml()
    g.create_grille()

    ready = False

    for k in range(30): # Attente 30 sc max 

        result = v1.list_namespaced_pod(namespace=result_1["namespace"])

        ready = True

        for pods in result.items:
            try:
                if pods.status.container_statuses[0].state.running == None:
                    ready = False
                    sleep(1)
            except:
                ready = False
                sleep(1)
            
        if ready:
            loader.stop()
            print("🎉🎉🎉 " + colored("KubeCondor est prêt vous pouvez accéder à la grille via ssh: \n\n", 'green', attrs=['bold']) + colored(f"ssh sge@{listnode.items[0].status.addresses[0].address} -p {g.port} \n\n", 'red', attrs=['bold']) + colored(f"mdp: sge\n\n", 'red', attrs=['bold']))
            return(1)

    print(colored("\n\nUne erreur est survenue veuillez vérifier la grille...", 'red', attrs=['bold']))

def main():

    f = Figlet(font='slant')
    print (f.renderText('KubeEpia'))

    try:
        config.load_kube_config()
        v1 = client.CoreV1Api() # Test connection
    except:
        raise ConnectionError("Erreur lors du contact avec la grille veuillez vérifier votre fichier de configuration et l'état de la grille")
    
    choice = inquirer.select(
            message="Choissisez votre service:",
            choices=["KubeVm", "KubeCondor"],
        ).execute()

    os.system('clear')

    if choice == "KubeVm":       
        vm()

    elif choice == "KubeCondor":
        htcondor()


    



if __name__ == "__main__":
    main()

