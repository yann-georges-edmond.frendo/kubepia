import yaml

from kubernetes import client, config, utils
import random

import os

class Vm:

    def __init__(self,name,cpu,ram,GPU_num=0,GPU_name=None,image=None):

        config.load_kube_config()
        v1 = client.CoreV1Api()

        list_namespace = v1.list_namespace(watch=False)

        flag_vm = False

        for i in list_namespace.items:
            if i.metadata.name == "vm":
                flag_vm = True
        
        if not flag_vm :
            v1.create_namespace(client.V1Namespace(metadata=client.V1ObjectMeta(name="vm"))) #Créer le ns au cas où

        list_vms = v1.list_namespaced_pod(namespace="vm")

        for i in list_vms.items:
            if i.metadata.name == name:
                raise NameError("Nom de vm déja utilisé veuillez en choisir un autre")


        list_nodeport = [] # liste des nodeport utilisé
        api_list_nodeport = v1.list_service_for_all_namespaces(watch=False)
        for i in api_list_nodeport.items:
            for j in i.spec.ports:
                list_nodeport.append(j._node_port)
        port = 30000
        while port in list_nodeport: #On tire un port non utilisé de la plage
            port = random.randint(30000,32768)

        self.name = name
        self.cpu_nodes = str(cpu)
        self.ram_nodes = str(ram) + '.0Gi' #Init ram par nodes en GO
        self.port = port
        self.image = image

        self.gpu_num = GPU_num
        self.gpu_name = GPU_name

        
    
    def create_yaml(self):

        with open('vm/vm.yaml', 'r') as file:
            prime_service = yaml.safe_load_all(file)

            for k in prime_service:

                if k["kind"] == "Pod":
                    vm_pod = k

                if k["kind"] == "Service":
                    ssh_service = k


        vm_pod["metadata"]["name"] = self.name
        ssh_service["metadata"]["name"] = self.name +  "-ssh"

        vm_pod["metadata"]["labels"]["app"] = self.name + "-app"
        ssh_service["spec"]["selector"]["app"] = self.name + "-app"

        vm_pod["spec"]["containers"][0]["resources"]["requests"]["cpu"] = self.cpu_nodes #Nombres de CPU
        vm_pod["spec"]["containers"][0]["resources"]["limits"]["cpu"] = self.cpu_nodes

        vm_pod["spec"]["containers"][0]["resources"]["requests"]["memory"] = self.ram_nodes #Memory
        vm_pod["spec"]["containers"][0]["resources"]["limits"]["memory"] = self.ram_nodes

        if self.gpu_num != 0:
            vm_pod["spec"]["nodeSelector"] = {"nvidia.com/gpu.product":self.gpu_name}
            vm_pod["spec"]["containers"][0]["resources"]["requests"]["nvidia.com/gpu"] = str(self.gpu_num)
            vm_pod["spec"]["containers"][0]["resources"]["limits"]["nvidia.com/gpu"] = str(self.gpu_num)


        ssh_service["spec"]["ports"][0]["nodePort"] = self.port

        if self.image is not None:
            vm_pod["spec"]["containers"][0]['image'] = self.image

        if not os.path.exists(path = './vm_yaml_files'):
            os.makedirs('./vm_yaml_files')
                
        
        with open(f"./vm_yaml_files/{self.name}.yaml","w+") as file:
        
            yaml.dump_all(
                [vm_pod,ssh_service],
                file,
                default_flow_style=False)

    def create_vm(self):
        """Permet de créer la grille en fonction du fichier YAML créer"""
        config.load_kube_config()
        k8s_client = client.ApiClient()
        yaml_file = f"./vm_yaml_files/{self.name}.yaml"
        utils.create_from_yaml(k8s_client, yaml_file)

